import {Action} from "redux";
import {MovieOrTVShow, MoviesState} from "../../interfaces/movies";

const initialState: MoviesState = { list: [], movieToDisplay: -1, searchedText: '', searchInProgress: false, movieDetails: undefined, favorites: [] };

export default function movies(state: MoviesState = initialState, action: {type: string, movies?: Array<MovieOrTVShow>, id?: number, text?: string, movieDetails?: string}) {
    switch (action.type) {
        case "STORE_MOVIES":
            return {...state, list: action.movies};
        case "DISPLAY_MOVIE":
            return {...state, movieToDisplay: action.id};
        case "SEARCHED_TEXT":
            return {...state, searchedText: action.text};
        case "CLEAR_MOVIES":
            return {...state, list: []};
        case "SEARCH_IN_PROGRESS":
            return {...state, searchInProgress: true};
        case "SEARCH_IS_DONE":
            return {...state, searchInProgress: false};
        case "STORE_MOVIE_DETAILS":
            return {...state, movieDetails: action.movieDetails};
        case "STORE_IN_FAVORITES":
            return {...state, favorites: [...state.favorites, action.id]};
        default:
            return state;
    }
}