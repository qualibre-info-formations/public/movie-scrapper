import {Dispatch} from "redux";

const BEARER_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzMjcxNWQ4YTI4MTkwMDNhNjhkOGQ1OWIzZTUzMDBkOCIsInN1YiI6IjYxYmIyZDA5ZTcyZmU4MDA5NWU2M2U1MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.ANdIC4WQqzpXfFU1V3bKTX8EJReIDo2LksbzZl1rRZI";

export const searchInTMDBAPI = (type: string, search: string) => {
    return (dispatch: Dispatch) => {
        dispatch({type: "SEARCH_IN_PROGRESS"});
        fetch(`https://api.themoviedb.org/3/search/${type}?query=${search}`, {
            headers: {'Authorization': `Bearer ${BEARER_TOKEN}`}
        }).then((result) => {
            if (result.ok) {
                return result.json()
            } else {
                dispatch({type: "SEARCH_IS_DONE"});
            }
        }).then((data) => {
            dispatch({type: 'STORE_MOVIES', movies: data.results});
            dispatch({type: "SEARCH_IS_DONE"});
        }).catch((err) => {
            dispatch({type: "SEARCH_IS_DONE"});
            console.error(err);
        });
    }
}

export const displayDetailsFoMovie = (id: number) => {
    return (dispatch: Dispatch, state: any) => {
        const store = state.getState();
        dispatch({type: "DISPLAY_MOVIE", id});
    }
}

export const getDetails = (type: string, id: number) => {
    return (dispatch: Dispatch) => {
        fetch(`https://api.themoviedb.org/3/${type}/${id}`, {
            headers: {'Authorization': `Bearer ${BEARER_TOKEN}`}
        }).then((result) => {
            if (result.ok) {
                return result.json()
            }
        }).then((data) => {
            dispatch({type: 'STORE_MOVIE_DETAILS', movieDetails: data});
        }).catch((err) => {
            console.error(err);
        });
    }
}