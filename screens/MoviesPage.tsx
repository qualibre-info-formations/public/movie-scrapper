import {FlatList, StyleSheet} from 'react-native';

import { View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import {MoviesState} from "../interfaces/movies";
import {useDispatch, useSelector} from "react-redux";
import {useEffect, useState} from "react";
import {searchInTMDBAPI} from "../actions/movies";
import MovieOrShowItem from "./MovieOrShowItem";
import {ButtonGroup, SearchBar} from "react-native-elements";

export default function MoviesPage({ navigation }: RootTabScreenProps<'Movies'>) {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [textInSearchBar, setTextInSearchBar] = useState('');
  const [latestChangeInSearch, setLatestChangeInSearch] = useState(0);
  const [changeTimeout, setChangeTimeout] = useState(undefined);
  const movies = useSelector((state: {movies: MoviesState}) => state.movies.list);
  const searchedText = useSelector((state: {movies: MoviesState}) => state.movies.searchedText);
  const searchInProgress = useSelector((state: {movies: MoviesState}) => state.movies.searchInProgress);
  const dispatch = useDispatch();

  useEffect(() => {
      if (searchedText) {
          dispatch(searchInTMDBAPI(selectedIndex === 0 ? 'movie' : 'tv', searchedText));
      } else {
          dispatch({type: "CLEAR_MOVIES"});
      }
  }, [selectedIndex, searchedText]);

  const updateSearch = (text: string) => {
      setTextInSearchBar(text);
      clearTimeout(changeTimeout);
      if (text.length > 3) {
          const now = Date.now();
          if (now - latestChangeInSearch > 1500) {
              dispatch({type: "SEARCHED_TEXT", text})
          } else {
              const timeout = setTimeout(() => {
                  dispatch({type: "SEARCHED_TEXT", text})
              }, 1500);
              // @ts-ignore
              setChangeTimeout(timeout);
          }
          setLatestChangeInSearch(now);
      }
  };

  const showDetails = (id: number) => {
      // @ts-ignore
      navigation.navigate('movieDetail', {media: id, type: selectedIndex === 0 ? 'movie' : 'tv'});
  };

    return (
    <View style={styles.container}>
        {/* @ts-ignore */ }
      <SearchBar lightTheme showLoading={searchInProgress} placeholder="Tapez votre recherche..." onChangeText={updateSearch} value={textInSearchBar}/>
      <ButtonGroup
          buttons={['Films', 'Séries']}
          selectedIndex={selectedIndex}
          onPress={(value) => {
            setSelectedIndex(value);
          }}
          containerStyle={{ marginBottom: 10 }}
      />
      <FlatList data={movies} renderItem={({item}) => <MovieOrShowItem showDetails={showDetails} media={item} />} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
