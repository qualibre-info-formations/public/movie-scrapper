import {FlatList, StyleSheet, TouchableOpacity, Image} from 'react-native';

import { Text, View } from '../components/Themed';
import {Genre} from "../interfaces/movies";
import {useDispatch, useSelector} from "react-redux";
import {getDetails} from "../actions/movies";
import {useEffect} from "react";
import {MaterialIcons} from "@expo/vector-icons";

export default function MovieOrShowDetails({route}: any) {
  const mediaId = route.params.media;
  const mediaType = route.params.type;
  const dispatch = useDispatch();
  const media = useSelector((state: {movies: {movieDetails: any}}) => state.movies.movieDetails);
  const favorites = useSelector((state: {movies: {favorites: Array<number>}}) => state.movies.favorites);

  useEffect(() => {
    dispatch(getDetails(mediaType, mediaId));
  }, [])

  useEffect(() => {
    console.log(media);
  }, [media]);

  const toggleFavorites = () => {
    dispatch({type: "STORE_IN_FAVORITES", id: mediaId});
  }

  return (
      <View style={styles.main_container}>
        {media ? (<>
        <View style={{flexDirection: 'row', justifyContent: "space-between"}}>
          <Image
              style={styles.image}
              source={{uri: `https://image.tmdb.org/t/p/original/${media.poster_path}`}}
          />
          <MaterialIcons style={{margin: 10}} onPress={toggleFavorites} name={favorites.includes(mediaId) ? "favorite" : "favorite-outline"} size={24} color="red" />
        </View>
        <View style={styles.content_container}>
          <View style={styles.header_container}>
            <Text style={styles.name_text}>{media.title ? media.title : media.name}</Text>
            <Text style={styles.price_text}>{media.popularity.toFixed(2)}</Text>
          </View>
          <View style={styles.description_container}>
          <Text>{media.tagline}</Text>
          </View>
          <View style={styles.description_container}>
            <Text style={styles.description_text} numberOfLines={6}>{media.overview}</Text>
          </View>
          <View style={styles.stock_container}>
            <Text>Genres : {media.genres.map((g: Genre) => g.name).join(', ')}</Text>
            <Text style={styles.stock_text}>Note : {media.vote_average}</Text>
          </View>
        </View>
        </>) : <></>}
      </View>
  );
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1
  },
  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },
  name_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  price_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
  stock_container: {
    flex: 1
  },
  stock_text: {
    textAlign: 'right',
    fontSize: 14
  }
});
