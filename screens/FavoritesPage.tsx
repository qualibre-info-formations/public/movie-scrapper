import {FlatList, StyleSheet} from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import {useSelector} from "react-redux";
import {MovieOrTVShow, MoviesState} from "../interfaces/movies";
import MovieOrShowItem from "./MovieOrShowItem";

export default function FavoritesPage({navigation}: {navigation: any}) {
  const movies = useSelector((state: {movies: MoviesState}) => state.movies.list);
  const favorites = useSelector((state: {movies: {favorites: Array<number>}}) => state.movies.favorites);
  // @ts-ignore
  const favoriteMovies: Array<MovieOrTVShow> = favorites.map(fav => movies.find(m => m.id === fav));

  const showDetails = (id: number) => {
    // @ts-ignore
    navigation.navigate('movieDetail', {media: id});
  };

  return (
    <View style={styles.container}>
      {favoriteMovies && favoriteMovies.length > 0 ? <FlatList data={favoriteMovies}  renderItem={({item}) => <MovieOrShowItem showDetails={showDetails} media={item} />} />
          : <Text>There is no favorites for now</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
