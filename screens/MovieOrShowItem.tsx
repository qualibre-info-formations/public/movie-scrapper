import {FlatList, StyleSheet, TouchableOpacity, Image} from 'react-native';

import { Text, View } from '../components/Themed';
import {MovieOrTVShow} from "../interfaces/movies";
import {useDispatch} from "react-redux";
import {displayDetailsFoMovie} from "../actions/movies";

export default function MovieOrShowItem({media, showDetails}: {media: MovieOrTVShow, showDetails: Function}) {
  return (
      <TouchableOpacity style={styles.main_container} onPress={() => showDetails(media.id)}>
        <Image
            style={styles.image}
            source={{uri: `https://image.tmdb.org/t/p/original/${media.poster_path}`}}
        />
        <View style={styles.content_container}>
          <View style={styles.header_container}>
            <Text style={styles.name_text}>{media.title ? media.title : media.name}</Text>
            <Text style={styles.price_text}>{media.popularity.toFixed(2)}</Text>
          </View>
          <View style={styles.description_container}>
            <Text style={styles.description_text} numberOfLines={6}>{media.overview}</Text>
          </View>
          <View style={styles.stock_container}>
            <Text style={styles.stock_text}>Note : {media.vote_average}</Text>
          </View>
        </View>
      </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  main_container: {
    height: 190,
    flexDirection: 'row'
  },
  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },
  name_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  price_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
  stock_container: {
    flex: 1
  },
  stock_text: {
    textAlign: 'right',
    fontSize: 14
  }
});
