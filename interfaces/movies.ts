export interface MovieOrTVShow {
    adult: boolean
    backdrop_path: string
    genre_ids: Array<number>
    id: number
    original_language: string
    original_title: string
    overview: string
    popularity: number
    poster_path: string
    release_date: string
    title: string
    video: boolean
    vote_average: number
    vote_count: number
    name: string
}

export interface Genre {
    id: number
    name: string
}

export interface MoviesState {
    list: Array<MovieOrTVShow>
    movieToDisplay: number
    searchedText: string
    searchInProgress: boolean
    favorites: Array<number>
    movieDetails: any
}
